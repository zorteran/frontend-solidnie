//window.addEventListener("load", function(event) {
//document.addEventListener('DOMContentLoaded', function(){
console.log('app start!');

var playlistTable = $('.playlist_table');

var playlistView = new ItemsView(playlistTable.find('tbody'), {
    counter: playlistTable.find('.counter'),
    makeItem: function makeRow(data) {
        return $('<tr data-id="' + data.i + '">\
                <td>'+ data.i + '</td>\
                <td>'+ data.title + '</td>\
                <td>'+ data.author + '</td>\
                <td class="del"> &times; </td>\
            </tr>')
    }
})

var albumsView = new ItemsView($('.music_search .card-deck'),{
    counter: $('<div>'),
    makeItem: function(data){
        return ' <div class="card">\
                <img class="card-img-top" src="'+data.images[2].url+'">\
                <div class="card-block">\
                    <h4 class="card-title">'+data.name+'</h4>\
                    <div class="card-text">'+data.artists[0].name+'</div>\
                </div>\
            </div>'
    }
})
var albumsModel = new ItemsModel([
    // { id: 1, img: 'http://placehold.it/150x150/', title:'Tytuł', text:'Opis 1...'},
    // { id: 2, img: 'http://placehold.it/150x150/', title:'Tytuł', text:'Opis 2...'},
    // { id: 3, img: 'http://placehold.it/150x150/', title:'Tytuł', text:'Opis 3...'}
],[albumsView])
albumsModel.notifyView();


var url = 'https://api.spotify.com/v1/search?type=album&market=PL&query=batman'

$.getJSON(url)
.then(function(data){ 
   albumsModel.items = data.albums.items;
   albumsModel.notifyView();
})

var playlistModel = new ItemsModel([{
    i: 1, title: 'Song', author: 'Batman'
}], [
        playlistView,
    ])
playlistModel.notifyView();


var playlistCtrl = new ItemsController(playlistTable.find('form'),
    playlistTable.find('tbody'), playlistModel)


//})